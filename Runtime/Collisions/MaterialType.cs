﻿namespace MisterGames.Common.Collisions {
    
    public enum MaterialType {
        Wood,
        Metal,
        Glass,
        Gravel,
        Stone,
        Water
    }
    
}