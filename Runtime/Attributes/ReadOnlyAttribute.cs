﻿using UnityEngine;

namespace MisterGames.Common.Attributes {

    public class ReadOnlyAttribute : PropertyAttribute { }

    public class BeginReadOnlyGroupAttribute : PropertyAttribute { }
 
    public class EndReadOnlyGroupAttribute : PropertyAttribute { }
    
}